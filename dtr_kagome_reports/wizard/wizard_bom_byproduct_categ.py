# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from odoo.exceptions import ValidationError


class WizardBomByProductCateg(models.TransientModel):
	_name = 'wizard.bom.byproduct.categ'

	prod_cat_ids = fields.Many2many('product.category','product_categ_rel', 'categ_id', 'id', domain=[('name', 'in', 
		['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services'])], string='Product Category')
	class_code_ids = fields.Many2many('mrp.bom','mrp_bom_rel','bom_id','id', string='Class Code')

	@api.onchange('prod_cat_ids')
	def onchange_prod_cat_ids(self):
		possible_class_code = []
		if self.prod_cat_ids:
			for prod_cat_id in self.prod_cat_ids:
				product_template_ids = self.env['product.template'].search([('categ_id','=', prod_cat_id.id)])
				for product_template in product_template_ids:
					for product_ids in self.env['mrp.bom'].search([('product_tmpl_id','=',product_template.id)]):
						for product_id in product_ids:
							possible_class_code.append(product_id.id)
		self.class_code_ids = self.env['mrp.bom'].search([('id','in',possible_class_code)])


	@api.multi
	def print_report(self):
		self.ensure_one()
		data = {}
		data['vals'] = {
			'prod_cat_ids': self.prod_cat_ids.ids or False,
			'class_code_ids': self.class_code_ids.ids or False,
		}

		return self.env['report'].get_action(self, 'dtr_kagome_reports.kagome_bom_report', data=data)

