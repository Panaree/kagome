# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime
from odoo.exceptions import ValidationError


class WizardMatCostVariance(models.TransientModel):
	_name = 'wizard.mat.cost.variance'


	prod_cat_ids = fields.Many2many('product.category','categ_cost_rel', 'categ_id', 'id', domain=[('name', 'in', 
		['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services'])], string='Product Category')
	to_date = fields.Date('To Date', required=True, copy=False, default=fields.Date.context_today)

	@api.multi
	def print_report(self):
		self.ensure_one()
		data = {}
		data['vals'] = {
			'prod_cat_ids': self.prod_cat_ids.ids or False,
			'to_date': self.to_date or fields.Date.context_today,
		}

		return self.env['report'].get_action(self, 'dtr_kagome_reports.kagome_matcost_variance_report', data=data)