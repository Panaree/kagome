# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime
from odoo.exceptions import ValidationError


class WizardPlannedFixedCostsbyCostCentre(models.TransientModel):
	_name = 'wizard.planned.fixedcosts.bycostcentre'

	to_date = fields.Date('To Date', required=True, copy=False, default=fields.Date.context_today)

	@api.multi
	def print_report(self):
		self.ensure_one()
		data = {}
		data['vals'] = {
			'to_date': self.to_date or fields.Date.context_today,
		}

		return self.env['report'].get_action(self, 'dtr_kagome_reports.kagome_planned_fixedcosts_report', data=data)