{
    'name': 'Kagome Reports',
    'version': '1.0',
    'summary': 'Kagome Management Reports by Dataroot Asia',
    'description': """
Kagome Reports
=============
    Kagome Management Reports by Dataroot Asia

- Sales Variance Report
- Purchases Variance Report
- Fixed Cost Variance Report for Cost Centre
- BOM Report
- Production Material Consumption Variance Report
- Production Material Cost Variance Report
- Production Variance Report

    """,
    'category': 'Tools',
    'author': 'Panaree Ngamnimit',
    'depends': ['account', 'analytic', 'sale', 'purchase', 'mrp'],
    'application': False,
    'data': [
        'security/ir.model.access.csv',
        'data/paper_format.xml',
        'views/actual_sale_line_view.xml',
        'views/actual_purchase_line_view.xml',
        'views/actual_fixedcost_line_view.xml',
        'views/actual_consumption_line_view.xml',
        'views/actual_matcost_line_view.xml',
        'views/actual_production_line_view.xml',

        'wizard/wizard_bom_byproduct_categ.xml',
        'report/kagome_bom_report.xml',

        'wizard/wizard_sale_variance_bydate.xml',
        'report/kagome_sale_variance_report.xml',

        'wizard/wizard_purchase_variance_bydate.xml',
        'report/kagome_purchase_variance_report.xml',

        'wizard/wizard_planned_fixedcosts_bycostcentre.xml',
        'report/kagome_planned_fixedcosts_report.xml',

        'wizard/wizard_consumption_variance.xml',
        'report/kagome_consumption_variance_report.xml',

        'wizard/wizard_mat_cost_variance.xml',
        'report/kagome_matcost_variance_report.xml',

        'wizard/wizard_production_variance.xml',
        'report/kagome_production_variance_report.xml',
        
    ],
}
