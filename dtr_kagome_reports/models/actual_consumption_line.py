# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from datetime import datetime
from odoo.exceptions import UserError


class ActualConsumptionLine(models.Model):
    _name = 'actual.consumption.line'

    input_date = fields.Date('Input Date', required=True, copy=False, default=fields.Date.context_today)
    input_user = fields.Many2one('res.users', string='Input User', required=True, default=lambda self: self.env.user)
    bom_id = fields.Many2one('mrp.bom', string='Product Produced', required=True)
    prod_month_qty = fields.Float(string='Current Month Production', default=0.0, digits=dp.get_precision('Product Price'))
    is_control = fields.Boolean(string='Control', default=False)
    product_id = fields.Many2one('product.product', string='Product')
    product_uom = fields.Many2one('product.uom', string='UOM')
    month_consumption = fields.Float(string='Current Month Actual Consumption', default=0.0, digits=dp.get_precision('Product Price'))

