# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from datetime import datetime
from odoo.exceptions import UserError


class ActualProductionLine(models.Model):
    _name = 'actual.production.line'

    input_date = fields.Date('Input Date', required=True, copy=False, default=fields.Date.context_today)
    input_user = fields.Many2one('res.users', string='Input User', required=True, default=lambda self: self.env.user)
    bom_id = fields.Many2one('mrp.bom', string='Product Produced', required=True)
    # spec_code = fields.Many2one('product.attribute.value', string='Spec Code')
    month_qty = fields.Float(string='Quantity', required=True, default=0.0, digits=dp.get_precision('Product Price'))
    month_amt = fields.Float(string='Amount', required=True, default=0.0, digits=dp.get_precision('Product Price'))