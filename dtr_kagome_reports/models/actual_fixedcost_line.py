# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from datetime import datetime
from odoo.exceptions import UserError


class ActualFixedcostLine(models.Model):
    _name = 'actual.fixedcost.line'


    input_date = fields.Date('Input Date', required=True, copy=False, default=fields.Date.context_today)
    input_user = fields.Many2one('res.users', string='Input User', required=True, default=lambda self: self.env.user)
    account = fields.Many2one('account.account', string='Account')
    cost_center = fields.Many2one('account.analytic.account', string='Cost Center')
    month_amt = fields.Float(string='Actual for the Month', required=True, default=0.0, digits=dp.get_precision('Product Price'))