# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime, date
from operator import itemgetter, attrgetter, methodcaller

class KagomePurchaseVarianceReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_purchase_variance_report'

	@api.multi
	def get_lines(self,data):
		res = []

		product_categ = ['Raw Materials', 'Packaging Materials', 'Utilities']
		if data['vals']['prod_cat_ids']:
			prod_cat_ids = self.env['product.category'].search([('id','in', data['vals']['prod_cat_ids'])])
		else:
			prod_cat_ids = self.env['product.category'].search([('name', 'in', product_categ)])

		to_date = data['vals']['to_date']
		ato_date = datetime.strptime(to_date, "%Y-%m-%d")
		from_date = date(ato_date.year, ato_date.month, 1)
		first_date_year = date(ato_date.year, 1, 1)

		actual_purchase_year = self.env['actual.purchase.line'].search([('input_date','<=',to_date), ('input_date','>=', first_date_year)])
		actual_purchase_month = self.env['actual.purchase.line'].search([('input_date','<=',to_date), ('input_date','>=', from_date)])

		query = '''SELECT prodt.id as product, SUM(ap.month_qty) as qty, 
						SUM(ap.month_amt) as amt
					FROM actual_purchase_line ap
					LEFT JOIN product_template prodt ON ap.product_id = prodt.id
					LEFT JOIN product_category categ ON prodt.categ_id = categ.id
					WHERE ap.id in %s AND prodt.categ_id = %s
					GROUP BY  prodt.id
					ORDER BY  prodt.id'''

		
		product_lines = []
		for category in prod_cat_ids:
			if actual_purchase_year:
				year_purchase = self.env.cr.execute(query, (tuple(actual_purchase_year.ids), category.id))
				for product_id,qty,amt in self.env.cr.fetchall():
					product = self.env['product.template'].search([('id','=', product_id)])
					product_lines.append({
					'product_id': product.id,
					'product_name': product.name,
					'details': {
						'category': product.categ_id.name,
						'uom': product.uom_po_id.name,
						'budget': product.standard_price,
						'month_amt': 0,
						'month_qty': 0,
						'year_qty': qty,
						'year_amt': amt,
						}
					})

			
			if actual_purchase_month:
				month_purchase = self.env.cr.execute(query, (tuple(actual_purchase_month.ids), category.id))
				for m_product_id,m_qty,m_amt in self.env.cr.fetchall():
					product = self.env['product.template'].search([('id','=', m_product_id)])
					for line in product_lines:
						if line['product_id'] == product.id:
							line['details']['month_qty'] = m_qty
							line['details']['month_amt'] = m_amt


			res.append({
				'category': category.name,
				'product_lines': product_lines,
				})

		return to_date, from_date, product_lines, res


	@api.multi
	def render_html(self, docids, data=None):
		to_date, from_date, product_lines, res = self.get_lines(data)
		return self.env['report'].render('dtr_kagome_reports.kagome_purchase_variance_report', {
											'to_date': to_date, 'from_date': from_date, 
											'product_lines': product_lines, 'res': res})
