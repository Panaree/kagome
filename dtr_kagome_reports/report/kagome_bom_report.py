# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime
from operator import itemgetter, attrgetter, methodcaller

class KagomeBomReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_bom_report'

	@api.multi
	def get_lines(self,data):
		res = []
		bom = []
		product_categ = ['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services']
		prod_cat_ids = self.env['product.category'].search([('id','in', data['vals']['prod_cat_ids'])]) or self.env['product.category'].search([('name', 'in', product_categ)])
		spec_code_ids = self.env['mrp.bom'].search([('id','in', data['vals']['class_code_ids'])], order="product_tmpl_id asc") or self.env['mrp.bom'].search([], order="product_tmpl_id asc")

		for bom_id in spec_code_ids:
			bom_lines = []
			for line in bom_id.bom_line_ids:				
				bom_lines.append({
					'product': line.product_id.name_get()[0][1],
					'categ': line.product_id.categ_id.name,
					'categ_id': line.product_id.categ_id.id,
					'qty': line.product_qty,
					'uom': line.product_uom_id.name,
					'std_cost': line.product_id.standard_price,
					'bom_id': bom_id.id
					})
			
			bom_lines = sorted(bom_lines, key=itemgetter("categ_id","product"))
			
			bom.append({
				'bom': bom_id.name_get()[0][1],
				'bom_qty': bom_id.product_qty,
				'bom_uom': bom_id.product_uom_id.name,
				'bom_lines': bom_lines,
				})

		return bom

	@api.multi
	def render_html(self, docids, data=None):
		bom = self.get_lines(data)
		return self.env['report'].render('dtr_kagome_reports.kagome_bom_report', {
											'bom': bom})