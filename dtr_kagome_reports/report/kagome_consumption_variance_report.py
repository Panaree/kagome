# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime, date
from operator import itemgetter, attrgetter, methodcaller

class KagomeConsumptionVarianceReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_consumption_variance_report'

	@api.multi
	def get_lines(self,data):

		product_categ = ['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services']
		if data['vals']['prod_cat_ids']:
			prod_cat_ids = self.env['product.category'].search([('id','in', data['vals']['prod_cat_ids'])])
		else:
			prod_cat_ids = self.env['product.category'].search([('name', 'in', product_categ)])

		to_date = data['vals']['to_date']
		ato_date = datetime.strptime(to_date, "%Y-%m-%d")
		from_date = date(ato_date.year, ato_date.month, 1)
		first_date_year = date(ato_date.year, 1, 1)

		actual_consumption_year = self.env['actual.consumption.line'].search([('input_date','<=',to_date), ('input_date','>=', first_date_year)])
		actual_consumption_month = self.env['actual.consumption.line'].search([('input_date','<=',to_date), ('input_date','>=', from_date)])
		
		query_year_controller =  '''SELECT SUM(acl.prod_month_qty),acl.bom_id
								FROM actual_consumption_line acl
								LEFT JOIN mrp_bom bom ON acl.bom_id = bom.id
								LEFT JOIN product_template prodt ON bom.product_tmpl_id = prodt.id
								WHERE acl.is_control = TRUE
								AND acl.id in %s
								AND prodt.categ_id in %s
								GROUP BY acl.bom_id'''

		query_month_controller = '''SELECT SUM(prod_month_qty)
										FROM actual_consumption_line
										WHERE id in %s
										AND bom_id = %s
										AND is_control = TRUE '''

		query_bom_join = '''SELECT boml.product_id,boml.product_uom_id, boml.product_qty,
									actual.product_id, actual.actual_consump
								FROM mrp_bom_line boml
								LEFT JOIN (SELECT SUM(month_consumption) as actual_consump, product_id
										FROM actual_consumption_line
										WHERE id in %s
										AND bom_id = %s
										AND is_control = False
										GROUP BY product_id) as actual 
										ON boml.product_id = actual.product_id
								WHERE bom_id = %s'''

		query_month_consumption = '''SELECT SUM(month_consumption)
								FROM actual_consumption_line
								WHERE id in %s
								AND bom_id = %s
								AND product_id = %s
								AND is_control = FALSE '''


		bom = []	
		if actual_consumption_year:
			year_controllers = self.env.cr.execute(query_year_controller, (tuple(actual_consumption_year.ids), tuple(prod_cat_ids.ids)))
			for sum_qty, bom_id in self.env.cr.fetchall(): 
				year_qty = sum_qty
				bom_id = self.env['mrp.bom'].search([('id','=', bom_id)])

				if actual_consumption_month:
					month_controllers = self.env.cr.execute(query_month_controller, (tuple(actual_consumption_month.ids), bom_id.id))
					month_qty = self.env.cr.fetchone()[0] or 0.0
				else:
					month_qty = 0.0

				bom_lines = []
				year_consumption = self.env.cr.execute(query_bom_join, (tuple(actual_consumption_year.ids), bom_id.id, bom_id.id))
				for boml_product_id, product_uom_id, boml_product_qty, actual_product_id, actual_consump in self.env.cr.fetchall():
					boml_product = self.env['product.product'].search([('id','=', boml_product_id)])
					boml_product_tmlp = boml_product.product_tmpl_id.categ_id
					product_uom = boml_product.uom_id 
					boml_product_qty = boml_product_qty
					year_actual_consump = actual_consump or 0.0

					if actual_consumption_month:
						month_consumption = self.env.cr.execute(query_month_consumption, (tuple(actual_consumption_month.ids), bom_id.id, boml_product.id))
						month_qty_consumption = self.env.cr.fetchone()[0] or 0.0 
					else:
						month_qty_consumption = 0.0

					bom_lines.append({
						'product': boml_product.name,
						'category': boml_product_tmlp.name,
						'category_id': boml_product_tmlp.id,
						'uom': product_uom.name,
						'month_bybom': boml_product_qty * month_qty,
						'month_actual': month_qty_consumption,
						'month_variance': (boml_product_qty * month_qty) - month_qty_consumption,
						'year_bybom': boml_product_qty * year_qty,
						'year_actual': year_actual_consump,
						'year_variance': (boml_product_qty * year_qty) - year_actual_consump,
						})

				bom_lines = sorted(bom_lines, key=itemgetter("category_id","product"))


				bom.append({
						'bom_id': bom_id.product_tmpl_id.name,
						'month_qty': month_qty,
						'year_qty': year_qty,
						'bom_lines': bom_lines,

						})

		bom = sorted(bom, key=itemgetter("bom_id"))

		return to_date, bom




	@api.multi
	def render_html(self, docids, data=None):
		to_date, bom = self.get_lines(data)
		return self.env['report'].render('dtr_kagome_reports.kagome_consumption_variance_report', {
											'to_date': to_date, 'bom': bom})
