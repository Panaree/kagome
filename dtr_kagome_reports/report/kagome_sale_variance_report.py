# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime, date
from operator import itemgetter
from collections import defaultdict, OrderedDict

class KagomeSaleVarianceReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_sale_variance_report'

	@api.multi
	def get_lines(self,data):
		res = []
		product_categ = ['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services']
		
		if data['vals']['prod_cat_ids']:
			prod_cat_ids = self.env['product.category'].search([('id','in', data['vals']['prod_cat_ids'])], order="name asc")
		else:
			prod_cat_ids = self.env['product.category'].search([('name', 'in', product_categ)], order="name asc")
		
		to_date = data['vals']['to_date']
		ato_date = datetime.strptime(to_date, "%Y-%m-%d")
		from_date = date(ato_date.year, ato_date.month, 1)
		first_date_year = date(ato_date.year, 1, 1)

		actual_sale_year = self.env['actual.sale.line'].search([('input_date','<=',to_date), ('input_date','>=', first_date_year)])
		actual_sale_month = self.env['actual.sale.line'].search([('input_date','<=',to_date),('input_date','>=', from_date)])

		planned_sale_year = self.env['sale.order'].search([('date_order','<=',str(to_date)), ('date_order','>=', str(first_date_year))])
		planned_sale_month = self.env['sale.order'].search([('date_order','<=',str(to_date)),('date_order','>=', str(from_date))])
		sol_year = self.env['sale.order.line'].search([('order_id','in', planned_sale_year.ids)])
		sol_month = self.env['sale.order.line'].search([('order_id','in', planned_sale_month.ids)])

		query_planned = ''' SELECT 
								pl.product_id, pl.date_start, pl.date_end, pl.fixed_price, planned.sum_qty
							FROM 
								product_pricelist_item pl
							LEFT JOIN  
								product_product prod ON pl.product_id = prod.id
							LEFT JOIN  
								product_template prodt ON prod.product_tmpl_id = prodt.id
							LEFT JOIN (
									SELECT 
										SUM(sol.product_uom_qty) as sum_qty, sol.price_unit, sol.product_id
									FROM 
										sale_order_line sol
									LEFT JOIN  
										product_product prod ON sol.product_id = prod.id
									LEFT JOIN  
										product_template prodt ON prod.product_tmpl_id = prodt.id
									WHERE 
										sol.order_partner_id = %s 
									AND 	
										prodt.categ_id = %s
									AND
										sol.id in %s
									GROUP BY 
										sol.price_unit, sol.product_id
							) AS planned ON pl.product_id = planned.product_id AND pl.fixed_price = planned.price_unit
							WHERE 
								pl.pricelist_id = %s
							AND 
								prodt.categ_id = %s '''

		query_actual = '''SELECT SUM(month_qty), (month_amt/month_qty) as unit_price
							FROM actual_sale_line
							WHERE id in %s
							AND partner_id = %s
							AND	product_id = %s
							AND	year = %s
							GROUP BY (month_amt  / month_qty)'''

		query_planned_month = ''' SELECT SUM(sol.product_uom_qty) as sum_qty
									FROM sale_order_line sol
									WHERE sol.order_partner_id = %s 
									AND sol.product_id = %s
									AND sol.id in %s
									AND sol.price_unit = %s'''

		channels = self.env['crm.team'].search([], order="name asc")
		for channel in channels:
			budget_volume = 0.0
			actual_volume = 0.0
			amount_volume_variance = 0.0
			amount_price_variance = 0.0
			year_budget_volume = 0.0
			year_actual_volume = 0.0
			year_amount_volume_variance = 0.0
			year_amount_price_variance = 0.0
			customer_lines = []
			sale_ids = channel.member_ids
			customers = self.env['res.partner'].search([('user_id','in', [sale_ids.ids])], order="name asc")
			for customer in customers:
				categ_lines = []
				for category in prod_cat_ids:
					lines = []
					if sol_year:
						planned_year = self.env.cr.execute(query_planned, (customer.id, category.id, tuple(sol_year.ids), customer.property_product_pricelist.id, category.id))
						for product_id, date_start, date_end, fixed_price, planned_qty in self.env.cr.fetchall():
							product = self.env['product.product'].search([('id','=', product_id)])
							adate_end = datetime.strptime(date_start, "%Y-%m-%d")
							year = str(adate_end.year)
							planned_price = fixed_price or 0.0
							planned_qty = planned_qty or 0.0
							planned_amt = (planned_price * planned_qty)

							month_planned_price = 0.0
							month_qty = 0.0
							month_planned_amt = 0.0
							if sol_month:
								planned_month = self.env.cr.execute(query_planned_month, (customer.id, product.id, tuple(sol_month.ids), planned_price))
								month_qty = self.env.cr.fetchone()[0] or 0.0
								month_planned_amt = month_qty * planned_price

							actual_qty = 0.0
							actual_price = 0.0
							actual_amt = 0.0
							if sol_month:
								actual_month = self.env.cr.execute(query_actual, (tuple(actual_sale_month.ids), customer.id, product.id, year))
								for qty, price in self.env.cr.fetchall():
									actual_qty = qty or 0.0
									actual_price = price or 0.0
									actual_amt = actual_qty * actual_price

							year_actual_qty = 0.0
							year_actual_price = 0.0
							year_actual_amt = 0.0
							year_actual_month = self.env.cr.execute(query_actual, (tuple(actual_sale_year.ids), customer.id, product.id, year))
							for qty, price in self.env.cr.fetchall():
								year_actual_qty = qty or 0.0
								year_actual_price = price or 0.0
								year_actual_amt = year_actual_qty * year_actual_price

							if year_actual_qty != 0 or planned_qty != 0:
								lines.append({
								'product': product.name,
								'spec_code': product.attribute_value_ids[0].name,
								'product_categ': category.name,
								'budget_vol': planned_qty,
								'budget_price': planned_price,
								'budget_amt': planned_amt,
								'month_budget_vol': month_qty,
								'month_budget_amt': month_planned_amt,
								'actual_vol': actual_qty,
								'actual_price': actual_price,
								'actual_amt': actual_amt,
								'year_actual_vol': year_actual_qty,
								'year_actual_price': year_actual_price,
								'year_actual_amt': year_actual_amt,
								})

								budget_volume = budget_volume + month_qty
								actual_volume = actual_volume + actual_qty
								amount_volume_variance = amount_volume_variance + ((actual_qty - month_qty) * planned_price)
								amount_price_variance = amount_price_variance + ((actual_price - planned_price) * actual_qty)
								year_budget_volume = year_budget_volume + planned_qty
								year_actual_volume = year_actual_volume + year_actual_qty
								year_amount_volume_variance = year_amount_volume_variance + ((year_actual_qty - planned_qty) * planned_price)
								year_amount_price_variance = year_amount_price_variance + ((year_actual_price - planned_price) * year_actual_qty)


					lines = sorted(lines, key=itemgetter("product","spec_code"))

					if lines:
						categ_lines.append({
							'category': category.name,
							'line': lines,
							})

				if categ_lines:
					customer_lines.append({
						'customer': customer.name,
						'categ_lines': categ_lines,
						})
			
			res.append({
				'channel': channel.name,
				'customer_lines': customer_lines,
				'budget_volume': budget_volume,
				'actual_volume': actual_volume,
				'amount_volume_variance': amount_volume_variance,
				'amount_price_variance': amount_price_variance,
				'year_budget_volume': year_budget_volume,
				'year_actual_volume': year_actual_volume,
				'year_amount_volume_variance': year_actual_volume,
				'year_amount_price_variance': year_amount_price_variance,
				})


		return to_date, from_date, res

	@api.multi
	def summary_by_product_category_compute(self,data):
		summary_by_product_categoty = {}

		budget_volume_month_dict = defaultdict(float)
		actual_volume_month_dict = defaultdict(float)
		amount_volume_variance_month_dict = defaultdict(float)
		amount_price_variance_month_dict = defaultdict(float)
		budget_volume_year_dict = defaultdict(float)
		actual_volume_year_dict = defaultdict(float)
		amount_volume_variance_year_dict = defaultdict(float)
		amount_price_variance_year_dict = defaultdict(float)
		
		for item in data:
			for frist_sub_item in item['customer_lines']:
				for second_sub_item in frist_sub_item['categ_lines']:
					product_category = str(second_sub_item['category'])
					for third_sub_item in second_sub_item['line']:
						budget_volume_month_dict[product_category] += third_sub_item['month_budget_vol']
						actual_volume_month_dict[product_category] += third_sub_item['actual_vol']
						amount_volume_variance_month_dict[product_category] += (( third_sub_item['actual_vol'] - third_sub_item['month_budget_vol'] ) * third_sub_item['budget_price'] )
						amount_price_variance_month_dict[product_category] += (( third_sub_item['actual_price'] - third_sub_item['budget_price'] ) * third_sub_item['actual_vol'] )
						budget_volume_year_dict[product_category] += third_sub_item['budget_vol']
						actual_volume_year_dict[product_category] += third_sub_item['year_actual_vol']
						amount_volume_variance_year_dict[product_category] += (( third_sub_item['year_actual_vol'] - third_sub_item['budget_vol'] ) * third_sub_item['budget_price'] )
						amount_price_variance_year_dict[product_category] += (( third_sub_item['year_actual_price'] - third_sub_item['budget_price'] ) * third_sub_item['year_actual_vol'] )

		budget_volume_month_result = dict(budget_volume_month_dict)
		actual_volume_month_result = dict(actual_volume_month_dict)
		amount_volume_variance_month_result = dict(amount_volume_variance_month_dict)
		amount_price_variance_month_result = dict(amount_price_variance_month_dict)
		budget_volume_year_result = dict(budget_volume_year_dict)
		actual_volume_year_result = dict(actual_volume_year_dict)
		amount_volume_variance_year_result = dict(amount_volume_variance_year_dict)
		amount_price_variance_year_result = dict(amount_price_variance_year_dict)

		summary_by_product_category_result = list()
		for product_category in budget_volume_month_result:
			summary_by_product_category = dict()
			if product_category:
				summary_by_product_category.update(
					{
						'product_category': product_category,
						'budget_volume_month': float(format(budget_volume_month_result[product_category], '.3f')),
						'actual_volume_month': float(format(actual_volume_month_result[product_category], '.3f')),
						'amount_volume_variance_month': float(format(amount_volume_variance_month_result[product_category], '.3f')),
						'amount_price_variance_month': float(format(amount_price_variance_month_result[product_category], '.3f')),
						'budget_volume_year': float(format(budget_volume_year_result[product_category], '.3f')),
						'actual_volume_year': float(format(actual_volume_year_result[product_category], '.3f')),
						'amount_volume_variance_year': float(format(amount_volume_variance_year_result[product_category], '.3f')),
						'amount_price_variance_year': float(format(amount_price_variance_year_result[product_category], '.3f'))
					})
				summary_by_product_category_result.append(summary_by_product_category)
				
		return summary_by_product_category_result

	@api.multi
	def render_html(self, docids, data=None):
		to_date, from_date, res = self.get_lines(data)
		summary_by_product_category_data = self.summary_by_product_category_compute(res)
		return self.env['report'].render('dtr_kagome_reports.kagome_sale_variance_report', {
											'to_date': to_date, 'from_date': from_date, 'res': res, 'summary_by_product_category': summary_by_product_category_data})