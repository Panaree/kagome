# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime, date
from operator import itemgetter, attrgetter, methodcaller

class KagomeProductionVarianceReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_production_variance_report'

	@api.multi
	def get_lines(self,data):
		res = []

		product_categ = ['Carrot', 'Apple', 'Pears', 'Beetroot', 'Dice', 'Paste', 'Food Services']
		if data['vals']['prod_cat_ids']:
			prod_cat_ids = self.env['product.category'].search([('id','in', data['vals']['prod_cat_ids'])])
		else:
			prod_cat_ids = self.env['product.category'].search([('name', 'in', product_categ)])

		to_date = data['vals']['to_date']
		ato_date = datetime.strptime(to_date, "%Y-%m-%d")
		from_date = date(ato_date.year, ato_date.month, 1)
		first_date_year = date(ato_date.year, 1, 1)

		actual_production_year = self.env['actual.production.line'].search([('input_date','<=',to_date), ('input_date','>=', first_date_year)])
		actual_production_month = self.env['actual.production.line'].search([('input_date','<=',to_date),('input_date','>=', from_date)])

		planned_production_year = self.env['mrp.production'].search([('date_planned_start','<=',str(to_date)), ('date_planned_start','>=', str(first_date_year))])
		planned_production_month = self.env['mrp.production'].search([('date_planned_start','<=',str(to_date)),('date_planned_start','>=', str(from_date))])


		query_year = ''' SELECT prodt.id, mrp.bom_id, SUM(mrp.product_qty), actual_prod.actual_qty, actual_prod.actual_amt
								FROM mrp_production mrp
								LEFT JOIN ( 
									SELECT actual.bom_id as bom_id ,SUM(actual.month_qty) as actual_qty, SUM(actual.month_amt) as actual_amt
									FROM actual_production_line actual
									LEFT JOIN mrp_bom bom ON bom.id = actual.bom_id
									WHERE actual.id in %s
									GROUP by actual.bom_id
								) as actual_prod ON mrp.bom_id = actual_prod.bom_id
								LEFT JOIN product_template prodt ON mrp.product_id = prodt.id
								WHERE prodt.categ_id = %s
								AND mrp.id in %s
								GROUP by prodt.id, mrp.bom_id, actual_prod.bom_id, actual_prod.actual_qty, actual_prod.actual_amt
								 '''

		query_month_actual = ''' SELECT SUM(actual.month_qty) as actual_qty, SUM(actual.month_amt) as actual_amt
										FROM actual_production_line actual
										WHERE actual.id in %s
										AND actual.bom_id = %s
										GROUP by actual.bom_id'''

		query_month_planned = '''SELECT SUM(product_qty)
										FROM mrp_production 
										WHERE id in %s
										AND bom_id = %s
										GROUP by bom_id'''


		for prod_cat_id in prod_cat_ids:
			sum_planned_qty_month = 0.0
			sum_actual_qty_month =  0.0
			sum_qty_var_month =  0.0
			sum_planned_cost_month =  0.0
			sum_actual_cost_month =  0.0
			sum_cost_var_month =  0.0
			sum_planned_qty_year =  0.0
			sum_actual_qty_year =  0.0
			sum_qty_var_year =  0.0
			sum_planned_cost_year =  0.0
			sum_actual_cost_year =  0.0
			sum_cost_var_year =  0.0
			
			categ_lines = []
			prod_product_qty = 0.0
			actual_qty = 0.0
			actual_amt = 0.0
			if actual_production_year:
				year_production = self.env.cr.execute(query_year, (tuple(actual_production_year.ids), prod_cat_id.id, (tuple(planned_production_year.ids))))
				for prodt_id, bom_id, yprod_product_qty, yactual_qty, yactual_amt in self.env.cr.fetchall():
					product_tmlp = self.env['product.template'].search([('id','=', prodt_id)])
					product = self.env['product.product'].search([('product_tmpl_id','=', product_tmlp.id)], limit=1)
					unit_budget_price = product_tmlp.standard_price or product.standard_price
					bom_id = self.env['mrp.bom'].search([('id','=', bom_id)])
					prod_product_qty = yprod_product_qty or 0.0
					actual_qty = yactual_qty or 0.0
					actual_amt = yactual_amt or 0.0

		
					if planned_production_month:
						month_matcost = self.env.cr.execute(query_month_planned, (tuple(planned_production_month.ids), bom_id.id))
						month_qty = self.env.cr.fetchone()
						if month_qty: 
							month_qty = month_qty[0]
						else:
							month_qty = 0.0
					else:
						month_qty = 0.0
					
					actual_month_qty = 0.0
					actual_month_amt = 0.0
					if actual_production_month:
						month_actual_prod = self.env.cr.execute(query_month_actual, (tuple(actual_production_month.ids), bom_id.id))
						for actual_month_qty, actual_month_amt in self.env.cr.fetchall():
							actual_month_qty = actual_month_qty or 0.0
							actual_month_amt = actual_month_amt or 0.0
						
					categ_lines.append({
						'product_tmlp': product_tmlp.name,
						'budget_cost': unit_budget_price,
						'planned_qty_month': month_qty,
						'actual_qty_month': actual_month_qty,
						'qty_var_month': month_qty - actual_month_qty,
						'planned_cost_month': float(month_qty) * unit_budget_price,
						'actual_cost_month': actual_month_amt,
						'cost_var_month': (float(month_qty) * unit_budget_price) - actual_month_amt,
						'planned_qty_year': prod_product_qty or  0.0,
						'actual_qty_year': actual_qty or  0.0,
						'qty_var_year': prod_product_qty - actual_qty or 0.0,
						'planned_cost_year': float(prod_product_qty) * unit_budget_price or  0.0,
						'actual_cost_year': actual_amt or  0.0,
						'cost_var_year': (float(prod_product_qty) * unit_budget_price) - actual_amt or 0.0,
						})

					sum_planned_qty_month += month_qty
					sum_actual_qty_month += actual_month_qty
					sum_qty_var_month += month_qty - actual_month_qty
					sum_planned_cost_month += float(month_qty) * unit_budget_price
					sum_actual_cost_month += actual_month_amt
					sum_cost_var_month += (float(month_qty) * unit_budget_price) - actual_month_amt
					sum_planned_qty_year += prod_product_qty or  0.0
					sum_actual_qty_year += actual_qty or  0.0
					sum_qty_var_year += prod_product_qty - actual_qty or 0.0
					sum_planned_cost_year += float(prod_product_qty) * unit_budget_price or  0.0
					sum_actual_cost_year += actual_amt or  0.0
					sum_cost_var_year += (float(prod_product_qty) * unit_budget_price) - actual_amt or 0.0

				res.append({
					'category': prod_cat_id,
					'cat_lines': categ_lines,
					'sum_planned_qty_month': sum_planned_qty_month,
					'sum_actual_qty_month': sum_actual_qty_month,
					'sum_qty_var_month': sum_qty_var_month,
					'sum_planned_cost_month': sum_planned_cost_month,
					'sum_actual_cost_month': sum_actual_cost_month,
					'sum_cost_var_month': sum_cost_var_month,
					'sum_planned_qty_year': sum_planned_qty_year,
					'sum_actual_qty_year': sum_actual_qty_year,
					'sum_qty_var_year': sum_qty_var_year,
					'sum_planned_cost_year': sum_planned_cost_year,
					'sum_actual_cost_year': sum_actual_cost_year,
					'sum_cost_var_year': sum_cost_var_year,
					})


			res = sorted(res, key=itemgetter("category"))


		return to_date, res



	@api.multi
	def render_html(self, docids, data=None):
		to_date, res = self.get_lines(data)
		return self.env['report'].render('dtr_kagome_reports.kagome_production_variance_report', {
											'to_date': to_date, 'res': res})