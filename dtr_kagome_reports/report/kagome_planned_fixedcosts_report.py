# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, models, fields, _
from datetime import datetime, date
from operator import itemgetter, attrgetter, methodcaller

class KagomePlannedFixedcostsReport(models.TransientModel):
	_name = 'report.dtr_kagome_reports.kagome_planned_fixedcosts_report'

	@api.multi
	def get_lines(self,data):
		res = []
		total_byaccount = []

		to_date = data['vals']['to_date']
		ato_date = datetime.strptime(to_date, "%Y-%m-%d")
		from_date = date(ato_date.year, ato_date.month, 1)
		first_date_year = date(ato_date.year, 1, 1)

		actual_planned_year = self.env['actual.fixedcost.line'].search([('input_date','<=', to_date), ('input_date','>=', first_date_year)])
		actual_planned_month = self.env['actual.fixedcost.line'].search([('input_date','<=', to_date), ('input_date','>=', from_date)])

		move_year = self.env['account.move.line'].search([('date','<=',to_date),('analytic_account_id','!=',False), ('date','>=', first_date_year)])
		move_month = self.env['account.move.line'].search([('date','<=',to_date),('analytic_account_id','!=',False),('date','>=', from_date)])

		cost_centers = self.env['account.analytic.account'].search([], order="name asc")
		exp_id = self.env['account.account.type'].search([('name','=','Expenses')], limit=1).id

		query_actual = '''SELECT SUM(f.month_amt)
						FROM actual_fixedcost_line f
						WHERE f.id in %s AND f.account = %s AND f.cost_center = %s'''

		query_planned = '''SELECT SUM(ml.debit)
						FROM account_move_line ml
						WHERE ml.id in %s AND ml.account_id = %s AND ml.analytic_account_id = %s'''

		query_sum_actual = '''SELECT SUM(f.month_amt)
						FROM actual_fixedcost_line f
						WHERE f.id in %s AND f.account = %s AND f.cost_center in %s'''

		query_sum_planned = '''SELECT SUM(ml.debit)
						FROM account_move_line ml
						WHERE ml.id in %s AND ml.account_id = %s AND ml.analytic_account_id in %s'''

		sum_account_codes = self.env['account.account'].search([('user_type_id','=',exp_id),('deprecated','!=',True),('code','<',80000)], order="code asc")
		for sum_account_code in sum_account_codes:

			if actual_planned_month:
				month_actual = self.env.cr.execute(query_sum_actual, (tuple(actual_planned_month.ids), sum_account_code.id, tuple(cost_centers.ids)))
				sum_m_actual = self.env.cr.fetchone()[0] or 0.0

			if actual_planned_year:
				year_actual = self.env.cr.execute(query_sum_actual, (tuple(actual_planned_year.ids), sum_account_code.id, tuple(cost_centers.ids)))
				sum_y_actual = self.env.cr.fetchone()[0] or 0.0

			if move_month:
				month_planned = self.env.cr.execute(query_sum_planned, (tuple(move_month.ids), sum_account_code.id, tuple(cost_centers.ids)))
				sum_m_budget = self.env.cr.fetchone()[0] or 0.0

			if move_year:
				year_planned = self.env.cr.execute(query_sum_planned, (tuple(move_year.ids), sum_account_code.id, tuple(cost_centers.ids)))
				sum_y_budget = self.env.cr.fetchone()[0] or 0.0


			total_byaccount.append({
				'account_code': sum_account_code.code,
				'account': sum_account_code.name,
				'm_budget': sum_m_budget,
				'm_actual': sum_m_actual,
				'y_budget': sum_y_budget,
				'y_actual': sum_y_actual,
				})
							
		for cost_center in cost_centers:
			fixedcost_lines = []
			account_codes = self.env['account.account'].search([('user_type_id','=',exp_id),('deprecated','!=',True),('code','<',80000)], order="code asc")
			for account_code in account_codes:
				m_budget = 0.0
				m_actual = 0.0
				y_budget = 0.0
				y_actual = 0.0

				if actual_planned_month:
					month_actual = self.env.cr.execute(query_actual, (tuple(actual_planned_month.ids), account_code.id, cost_center.id))
					m_actual = self.env.cr.fetchone()[0] or 0.0

				if actual_planned_year:
					year_actual = self.env.cr.execute(query_actual, (tuple(actual_planned_year.ids), account_code.id, cost_center.id))
					y_actual = self.env.cr.fetchone()[0] or 0.0

				if move_month:
					month_planned = self.env.cr.execute(query_planned, (tuple(move_month.ids), account_code.id, cost_center.id))
					m_budget = self.env.cr.fetchone()[0] or 0.0

				if move_year:
					year_planned = self.env.cr.execute(query_planned, (tuple(move_year.ids), account_code.id, cost_center.id))
					y_budget = self.env.cr.fetchone()[0] or 0.0

				if y_actual != 0.0 or y_budget != 0.0:
					fixedcost_lines.append({
						'account_code': account_code.code,
						'account_name': account_code.name,
						'm_budget': m_budget,
						'm_actual': m_actual,
						'y_budget': y_budget,
						'y_actual': y_actual,
						})

			res.append({
				'cost_center': cost_center.name,
				'lines': fixedcost_lines,
				})

			# import pprint
			# pprint.pprint(total_byaccount) 

		return to_date, res, total_byaccount

	@api.multi
	def render_html(self, docids, data=None):
		to_date, res, total_byaccount = self.get_lines(data)
		return self.env['report'].render('dtr_kagome_reports.kagome_planned_fixedcosts_report', {
											'to_date': to_date, 'res': res, 'total_byaccount': total_byaccount,})
